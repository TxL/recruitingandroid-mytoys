package com.mytoys.recruitingandroid;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    WebView webView;

    private DrawerLayout mdrawer;
    private NavigationView mNavigation;
    private ListView mDrawerList;

    ArrayList<ListRecord> records;
    ArrayList<JsonObject> jsonResults;
    ArrayList<String> titles;

    // app lifecycle methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        mdrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mdrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mdrawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigation = (NavigationView) findViewById(R.id.nav_view);
        mDrawerList = (ListView) mNavigation.findViewById(R.id.left_drawer);

        webView = (WebView)findViewById(R.id.my_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= 21) {
            webView.getSettings().setMixedContentMode(webView.getSettings().MIXED_CONTENT_ALWAYS_ALLOW);
        }

        webView.setWebViewClient(new MyWebViewClient((ProgressBar) findViewById(R.id.progress1)));
        webView.loadUrl(getString(R.string.main_url).toString());


        records = new ArrayList<ListRecord>();
        mDrawerList.setAdapter(new CustomArrayAdapter(this,R.layout.drawer_list_item,records));
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                listItemClicked(pos);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        new JSONAsyncTask().execute();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (jsonResults.size() > 1) {

                while (jsonResults.size() > 1) {
                    jsonResults.remove(jsonResults.size()-1);
                    titles.remove(titles.size()-1);
                }

                reloadJsonData(jsonResults.get(jsonResults.size()-1), titles.get(titles.size()-1));
                webView.loadUrl(getString(R.string.main_url).toString());
            }
            else super.onBackPressed();
        }
    }

    // action methods

    private void listItemClicked(int pos) {

        ListRecord rec = records.get(pos);

        if (rec.type.equals("link")) {
            Log.d("XXX","clicked link");

            webView.loadUrl(rec.link);
            mdrawer.closeDrawers();
        }
        else if (rec.type.equals("external-link")) {
            Log.d("XXX","clicked external-link");

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rec.link));
            startActivity(browserIntent);

            mdrawer.closeDrawers();
        }
        else if (rec.type.equals("node")) {
            Log.d("XXX","clicked node");

            JsonObject newObj = getChildrens(jsonResults.get(jsonResults.size()-1),rec.item);
            jsonResults.add(newObj);
            titles.add(rec.item);
            reloadJsonData(jsonResults.get(jsonResults.size()-1), rec.item);
        }
    }

    public void closeDrawer(View v) {

        mdrawer.closeDrawers();
    }

    public void backDrawer(View v) {

        if (jsonResults.size() > 1) {

            jsonResults.remove(jsonResults.size()-1);
            titles.remove(titles.size()-1);
            reloadJsonData(jsonResults.get(jsonResults.size()-1), titles.get(titles.size()-1));
        }
    }

    // json data methods

    private void updateNavigationHeader(String title) {

        RelativeLayout rel = (RelativeLayout) mNavigation.findViewById(R.id.myHeader);

        Button backB = (Button) rel.findViewById(R.id.backBut);
        TextView txtTitleNav = (TextView) rel.findViewById(R.id.txtTitleNav);
        ImageView imageView = (ImageView) rel.findViewById(R.id.imageView);

        if (jsonResults.size() == 1) {

            backB.setVisibility(View.GONE);
            txtTitleNav.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
        }
        else if (jsonResults.size() > 1) {

            backB.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            txtTitleNav.setVisibility(View.VISIBLE);
            txtTitleNav.setText(title);
        }
    }

    private void reloadJsonData(JsonObject obj, String title) {

        records = new ArrayList<ListRecord>();

        JsonArray array = obj.getAsJsonArray("navigationEntries");
        JsonObject aux;

        for (int i = 0; i < array.size(); i++) {

            aux = (JsonObject) array.get(i);
            String type = aux.get("type").getAsString();
            String label = aux.get("label").getAsString();

            if ((type.equals("link")) || (type.equals("external-link")))
                records.add(new ListRecord(label,type,aux.get("url").getAsString()));
            else records.add(new ListRecord(label,type,""));

            if (type.equals("section"))
                extractNodes(aux);
        }

        Log.d("XXX",records.size () + "");
        Log.d("XXX",records.toString());

        mDrawerList.setAdapter(new CustomArrayAdapter(this,R.layout.drawer_list_item,records));

        updateNavigationHeader(title);
    }

    private void extractNodes(JsonObject obj) {

        JsonArray array = obj.getAsJsonArray("children");
        JsonObject aux;

        for (int i = 0; i < array.size(); i++) {

            aux = (JsonObject) array.get(i);
            String type = aux.get("type").getAsString();
            String label = aux.get("label").getAsString();

            if ((type.equals("link")) || (type.equals("external-link")))
                records.add(new ListRecord(label,type,aux.get("url").getAsString()));
            else records.add(new ListRecord(label,type,""));
        }
    }

    private JsonObject getChildrens(JsonObject current, String nodeName) {

        JsonObject result = null;

        int res = 0;
        JsonArray array = current.getAsJsonArray("navigationEntries");
        JsonObject aux, aux2;

        for (int i = 0; i < array.size(); i++) {

            aux = (JsonObject) array.get(i);
            String type = aux.get("type").getAsString();
            String label = aux.get("label").getAsString();

            if (res == 0) {
                if ((type.equals("node")) && (label.equals(nodeName))) {
                    res = 1;
                    result = new JsonObject();
                    result.add("navigationEntries",aux.getAsJsonArray("children"));
                }
            }

            if (res == 0) {

                JsonArray array2 = aux.getAsJsonArray("children");
                for (int j = 0; j < array2.size(); j++) {

                    aux2 = (JsonObject) array2.get(j);
                    String type2 = aux2.get("type").getAsString();
                    String label2 = aux2.get("label").getAsString();

                    if ((type2.equals("node")) && (label2.equals(nodeName))) {
                        res = 1;
                        result = new JsonObject();
                        result.add("navigationEntries",aux2.getAsJsonArray("children"));
                    }
                }
            }
        }

        return result;
    }

    // classes for webview and asyncrequest

    private class MyWebViewClient extends WebViewClient {

        private ProgressBar progressBar;

        public MyWebViewClient(ProgressBar progressBar) {
            this.progressBar=progressBar;
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }
    }

    private class JSONAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urls) {

            String responseString = null;
            try {
                URL url = new URL(getString(R.string.api_url).toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setRequestProperty("x-api-key", getString(R.string.api_key).toString());

                if(conn.getResponseCode() == HttpsURLConnection.HTTP_OK){
                    // Do normal input or output stream reading
                    responseString = "OK"; // See documentation for more info on response handling

                    InputStream inputStr = conn.getInputStream();
                    Scanner s = new Scanner(inputStr).useDelimiter("\\A");
                    String result = s.hasNext() ? s.next() : "";

                    responseString = result;
                }
                else {
                    responseString = "FAILED"; // See documentation for more info on response handling
                }
            } catch (IOException e) {
                //TODO Handle problems..
            }

            return responseString;
        }

        protected void onPostExecute(String result) {
           // Log.d("XXX",result);

            if (!result.equals("FAILED")) {

                Gson gson = new Gson();
                JsonElement element = gson.fromJson (result, JsonElement.class);

                jsonResults = new ArrayList<>();
                jsonResults.add(element.getAsJsonObject());

                titles = new ArrayList<>();
                titles.add("");

                reloadJsonData(jsonResults.get(0), "");
            }
        }
    }

}
