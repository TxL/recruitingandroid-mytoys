package com.mytoys.recruitingandroid;

public class ListRecord {

    public String item;
    public String type;
    public String link;

    public ListRecord(String item, String type, String link)
    {
        this.item = item;
        this.type = type;
        this.link = link;
    }

}

