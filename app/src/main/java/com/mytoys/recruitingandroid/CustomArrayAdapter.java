package com.mytoys.recruitingandroid;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by txlpro on 17/9/16.
 */
public class CustomArrayAdapter extends ArrayAdapter<ListRecord> {

    Context context;
    int layoutResourceId;
    ArrayList<ListRecord> data = null;

    public CustomArrayAdapter(Context context, int layoutResourceId, ArrayList<ListRecord> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ListHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);

            row.setTag(holder);
        }
        else
        {
            holder = (ListHolder)row.getTag();
        }

        ListRecord list = data.get(position);
        holder.txtTitle.setText(list.item);

        holder.imgIcon.setVisibility(View.VISIBLE);
        if (list.type.equals("node"))
            holder.imgIcon.setImageResource(R.drawable.arrow);
        else if (list.type.equals("external-link"))
            holder.imgIcon.setImageResource(R.drawable.external);
        else holder.imgIcon.setVisibility(View.INVISIBLE);

        if (list.type.equals("section")) {
            holder.txtTitle.setTypeface(null, Typeface.BOLD);
            row.setBackgroundColor(Color.LTGRAY);
        }
        else {
            holder.txtTitle.setTypeface(null, Typeface.NORMAL);
            row.setBackgroundColor(Color.TRANSPARENT);
        }

        return row;
    }

    public boolean isEnabled(int position) {
        ListRecord list = data.get(position);
        if (list.type.equals("section"))
            return false;
        else return true;
    }

    static class ListHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
