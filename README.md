Recruiting Android MyToys app - Jose Luis Gonzalez Moreno

  - Developed in Java with Android Studio 2.1.2 for Android API 19 (KitKat).
  - Native UI designed with MenuDrawer and Navigation Bar, just tested on phone devices.
  - One Activity Main, it handles JSON data request and inflate MenuDrawer ListView.
  - Asynchronous REST requests, JSON parse with native Google GSON library, Gradle handle dependencies.